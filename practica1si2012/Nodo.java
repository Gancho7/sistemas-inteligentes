/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package practica1si2012;

/**
 * Clase Nodo donde se guardarán los valores de las coordenadas x(fila) e y(columnas)
 * además de los valores de g y f que nos servirán para el algoritmo A*
 * 
 * @author Joaquín Vera Herrero
 */
public class Nodo {
    
    private static final Nodo nodoError = new Nodo(-1,-1);
    
    /******PRIVATE******/
    
    /**
     * fila
     */
    private int x;
    
    /**
     * columna 
     */
    private int y;
    
    /**
     * Valor de la longitud del camino que llevamos recorrido
     */
    private int g=0;
    
    /**
     * Valor de la fórmula F() que es la función óptima para el camino más corto
     */
    private int f=0;
    
    /**
     * Es el orden en el que ha sido explorado el nodo
     */
    private int ordenExplo=0;
    
    /*******PUNTERO A NODO******/
    public Nodo padre;
    
    
    
    /******CONSTRUCTORES******/
    
    /**
     * Constructor por defecto
     */
    public Nodo(){
        this.y=-1;
        this.x=-1;
    }
    
    /**
     * Constructor de copia
     * @param x coordenada x
     * @param y coordenada y
     */
    public Nodo(int x, int y){
        this.x=x;
        this.y=y;
    }
    
    
    /**
     * Constructor de copia
     * @param n 
     */
    public Nodo(Nodo n){
        if(n.x<=1 && n.y<=1){
            this.x=n.x;
            this.y=n.y;
        }else{this.x=-1;this.y=-1;}
    }
    
    
    
    /******GETTERS Y SETTERS******/
    
    
    /**
     * getter que devuelve el valor de y
     * @return devuelve el valor de y 
     */
    public int getY(){
        return this.y;
    }
    
    /**
     * Setter para sustituir nuestro valor de y
     * @param y se sustitulle el valor de y por este parámetro
     */
    public void setY(int y){
        if(y<=1){
            this.y=y;
        }else{this.x=-1;this.y=-1;}
    }
    
    /**
     * getter que devuelve el valor de x
     * @return devuelve el valor de x
     */
    public int getX(){
        return this.x;
    }
    
    /**
     * Setter para sustituir nuestro valor de x
     * @param x se sustitulle el valor de x por este parámetro
     */
    public void setX(int x){
        if(x<=1){
            this.x=x;
        }else{this.x=-1;this.y=-1;}
    }
    
    /**
     * getter que devuelve el valor de g
     * @return devuelve el valor de g
     */
    public int getG(){
        return this.g;
    }
    
    /**
     * Setter para sustituir nuestro valor de g
     * @param g se sustitulle el valor de g por este parámetro
     */
    public void setG(int g){
        this.g=g;
    }
    
    /**
     * getter que devuelve el valor de f
     * @return devuelve el valor de f
     */
    public int getF(){
        return this.f;
    }
    
    /**
     * Setter para sustituir nuestro valor de f
     * @param f se sustitulle el valor de f por este parámetro
     */
    public void setF(int f){
        this.f=f;
    }
    
    /**
     * Getter que devuelve el valor del orden de exploración
     * @return devuelve el valor del orden de explración
     */
    public int getOrden(){
        return this.ordenExplo;
    }
    
    /**
     * Setter para modificar el orden de exploración
     * @param orden nuevo orden de exploración
     */
    public void setOrden(int orden){
        this.ordenExplo=orden;
    }
    
    
    /******OTROS MÉTODOS******/
    
    /**
     * Método para comprobar si son iguales dos nodods
     * @param obj Es un objeto que nos puedan pasar
     * @return devuelve true o false si son o no iguales
     */
    public boolean equals(Object obj){
        boolean iguales=false;
        
        if(obj == this) // Los dos objetos son iguales
            return true;

        else if(obj == null) //El objeto pasado por parámetro es Nulo
            return iguales;

        if(!(obj instanceof Nodo)) // El parametro no es una instancia de esta clase.
            return iguales;

        Nodo n = (Nodo) obj; // este casting es necesario para convertir el objeto a Nodo,
                                         // en el caso de no serlo, y así compararlo

        if(this.x == n.x && this.y == n.y)
            iguales=true;

        return iguales;
    }
    
}
