/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package practica1si2012;

import java.util.ArrayList;
import javax.media.j3d.Transform3D;
import javax.vecmath.Point3d;
import simbad.sim.*;
import javax.vecmath.Vector3d;
import javax.vecmath.Matrix3d;


/**
 *
 * @author mireia
 */
public class MiRobot extends Agent{

        //Variables utilizadas para el controlador difuso
        RangeSensorBelt sonars;
        FuzzyController controller;
        
        //Variables generales
        int mundo[][]; //Datos del entorno
        int origen; //Punto de partida del robot. Será la columna 1 y esta fila
        int destino; //Punto de destino del robot. Será la columna tamaño-1 y esta fila
        char camino[][]; //Camino que debe seguir el robot. Será el resultado del A*
        int expandidos[][]; //Orden de los nodos expandidos. Será el resultado del A*
        int tamaño; //Tamaño del mundo


        
        public MiRobot(Vector3d position, String name, Practica1 practica1) {
            super(position, name);

            //Prepara las variables
            tamaño = practica1.tamaño_mundo;

            mundo = new int[tamaño][tamaño];
            camino = new char[tamaño][tamaño];
            expandidos = new int[tamaño][tamaño];
            origen = practica1.origen;
            destino = practica1.destino;
            mundo = practica1.mundo;
            
            //Inicializa las variables camino y expandidos donde el A* debe incluir el resultado
            for(int i=0;i<tamaño;i++)
                for(int j=0;j<tamaño;j++){
                    camino[i][j] = '.';
                    expandidos[i][j] = -1;
                }

            // Añade sonars
            sonars = RobotFactory.addSonarBeltSensor(this); // de 0 a 1.5m
            
        }

        //Calcula el A*
        public int AEstrella(){
            int result = 0;
            boolean encontrado=false;
            
             //Se almacenan los caminos con los nodos solución
            ArrayList listaInterior = new ArrayList<Nodo>();
             //se meten los nodos a explorar, los adyacentes a la situación actual
            ArrayList listaFrontera = new ArrayList<Nodo>();
            //Se guardan los nodo adyacentes
            ArrayList listaAdyacentes = new ArrayList<Nodo>();
            
            Nodo nOrigen = new Nodo(origen,1);
            Nodo nDestino = new Nodo(destino, tamaño-1);
            Nodo n; //Se irá guardando el mejor nodo
            Nodo nAux; //nodo para comprobar la listaAdyacentes y mostrar camino
            int gNueva; //Contendrá la g en este punto de los nodos adyacentes y se compara con
                        //los que SI estén en la listaFrontera para ver si se mejora el camino
            int orden=-1; //para que el primer orden del primer nodo sea 0
            listaFrontera.add(nOrigen);// Iniciamos la lista con el nodo origen
            
            
            
            while(!listaFrontera.isEmpty() && !encontrado){
                n=mejorNodo(listaFrontera);
                listaFrontera.remove(n);
                listaInterior.add(n);
                orden++;
                n.setOrden(orden);
                expandidos[n.getX()][n.getY()]=n.getOrden();
                if(n.getX()==nDestino.getX() && n.getY()==nDestino.getY()-1){ //DESTINO
                    //Actualizamos la matriz camino
                    nAux=(Nodo)listaInterior.get(listaInterior.size()-1);
                    while(!nAux.equals((Nodo)listaInterior.get(0))){
                        camino[nAux.getX()][nAux.getY()]='X';
                        nAux=nAux.padre;
                    }
                    
                    //Actualizamos la matriz de Expandidos
                    for(int i=0;i<listaInterior.size();i++){
                        nAux=(Nodo)listaInterior.get(i);
                        expandidos[nAux.getX()][nAux.getY()]=nAux.getOrden();
                    }
                    encontrado=true;   
                }
                
                //Calculamos adyacentes del nodo n
                listaAdyacentes=calculaAdyacentes(n, nDestino, listaInterior);
                
                for(int i=0;i<listaAdyacentes.size();i++){
                    nAux = (Nodo)listaAdyacentes.get(i);
                    if(!nAux.equals(nDestino)){ //No es destino
                        gNueva=n.getG()+1; //para comprobar en el caso de que esté en listaFrontera
                        if(noEnListaFrontera(listaFrontera,nAux)){
                            listaFrontera.add(nAux); //lo añadimos
                        }
                        else{ 
                            if(gNueva<nAux.getG()){
                                nAux.padre=n; // sustituimos su anterior padre
                                nAux.setG(n.getG()+1); //Recalculamos g a partir del nodo actual n
                                nAux.setF(F(nAux,nDestino));
                            }
                        }
                    }else{i=listaAdyacentes.size();} //Es destino y nos salimos del for
                }  
            }
            
            mostrarCamino();
            mostrarExpandidos();
            return result;
        }
        
        /**
         * Método para mostrar camino
         */
        public void mostrarCamino(){
            System.out.println("Camino");
            for(int i=0;i<tamaño-1;i++){
                for(int j=0;j<tamaño-1;j++){
                    System.out.print(" "+camino[i][j]);
                }
                System.out.println();
            }
        }
        
        /**
         * Método para mostrar los nodos expandidos
         */
        public void mostrarExpandidos(){
            System.out.println("Expandidos");
            for(int i=0;i<tamaño-1;i++){
                for(int j=0;j<tamaño-1;j++){
                    if(expandidos[i][j]<=9 && expandidos[i][j]>=0){
                        System.out.print("  "+expandidos[i][j]);
                    }else{System.out.print(" "+expandidos[i][j]);}
                }
                System.out.println();
            }
        }
        /**
         * Método que calcula el mejor nodo de la lista Frontera
         * @param listaFrontera lista donde están los candidatos a meter en listaInterior
         * @return devuelve el nodo con f() menor
         */
        public Nodo mejorNodo(ArrayList listaFrontera){
            Nodo nodoMejor = new Nodo();
            Nodo nAux = new Nodo(); //auxiliar
            int costeMejor;
            
            
            nodoMejor=(Nodo)listaFrontera.get(0); //Sacamos el primer nodo y lo asignamos como mejor
                    //costeMejor=F(nodoMejor, nDestino);
                    // el coste del primer nodo es el mejor ya que no lo hemos comparado con ningún otro
            costeMejor=nodoMejor.getF();
                    // el coste del primer nodo es el mejor ya que no lo hemos comparado con ningún otro
                    //Si listaFrontera solo tiene un nodo, el mejor nodo es el que hay
            if(listaFrontera.size()==1){
                 return nodoMejor;   
                }
            
            //Comparamos con todos los nodos de la listaFrontera
            for (int i=1; i<listaFrontera.size();i++){
                nAux=(Nodo)listaFrontera.get(i);
                if(nAux.getF()<costeMejor){
                    nodoMejor=nAux;}
            }
            
            return nodoMejor;
        }
        
        /**
         * Calcula la función F() mediante la heurística y la g (coste) que incluye el nodo
         * @param n nodo desde el que calcular el camino más corto
         * @param nDestino nodo destino
         * @return devuelve el coste total al coger este nodo
         */
        public int F(Nodo n, Nodo nDestino){
            int coste, h;
	    //double h,coste;
            
	    /**
            //Si ya tiene calculada F() no hace falta que la calcule de nuevo
            if(n.getF()!=0){
                return n.getF();}**/
            
            //Calculamos la heurística h() mediante una resta absoluta de las componentes x e y
            //esto nos da los pasos de cuadrados que tenemos que dar.
            h=Math.abs(nDestino.getX()-n.getX());
            h=h+Math.abs(nDestino.getY()-n.getY());
	    
	    //h=Math.sqrt(Math.pow(Math.abs(nDestino.getX()-n.getX()), 2)+Math.pow(Math.abs(nDestino.getY()-n.getY()), 2));
            
            coste=h+n.getG();
            n.setF(coste); //Actualizamos f de este nodo para no calcularlo más
            
            return coste;
        }

        /**
         * Método que calcula los nodos adyacentes a n y los devuelve en un ArrayList
         * @param n nodo actual
         * @param nDestino nodo de la posición destino. Utilizado para calcular la F()
         * @param listaInterior lista donde se van metiendo los nodo de la solción
         */
        public ArrayList calculaAdyacentes(Nodo n,Nodo nDestino, ArrayList listaInterior){
            ArrayList nodosAux = new ArrayList<Nodo>();
            
            
            Nodo nAtras = new Nodo(n.getX(),n.getY()-1);
            if(mundo[nAtras.getX()][nAtras.getY()]==0 && noEnListaInterior(listaInterior,nAtras)){
                nodosAux.add(nAtras);  //Metemos los nodos en este ArrayList para simplificar código
                nAtras.setG(n.getG()+1);
                nAtras.setF(F(nAtras,nDestino));
                nAtras.padre=n;} //Actualizamos el padre del nodo
            
            Nodo nDelante = new Nodo(n.getX(),n.getY()+1);
            if(mundo[nDelante.getX()][nDelante.getY()]==0 && noEnListaInterior(listaInterior,nDelante)){
                nodosAux.add(nDelante);//Metemos los nodos en este ArrayList para simplificar código
                nDelante.setG(n.getG()+1);
                nDelante.setF(F(nDelante,nDestino));
                nDelante.padre=n;} //Actualizamos el padre del nodo
            
            Nodo nArriba = new Nodo(n.getX()-1,n.getY());
            if(mundo[nArriba.getX()][nArriba.getY()]==0 && noEnListaInterior(listaInterior,nArriba)){
                nodosAux.add(nArriba);//Metemos los nodos en este ArrayList para simplificar código
                nArriba.setG(n.getG()+1);
                nArriba.setF(F(nArriba,nDestino));
                nArriba.padre=n;} //Actualizamos el padre del nodo
            
            Nodo nAbajo = new Nodo(n.getX()+1,n.getY());
            if(mundo[nAbajo.getX()][nAbajo.getY()]==0 && noEnListaInterior(listaInterior,nAbajo)){
                nodosAux.add(nAbajo);//Metemos los nodos en este ArrayList para simplificar código
                nAbajo.setG(n.getG()+1);
                nAbajo.setF(F(nAbajo,nDestino));
                nAbajo.padre=n;} //Actualizamos el padre del nodo
            

            return nodosAux;
        }
        
        /**
         * Comprueba si un nodo está en la listaInterior
         * @param listaInterior lista donde está el camino
         * @param n nodo a comprobar
         * @return devuelte true si NO está en la lista y false SI está en la lista
         */
        public boolean noEnListaInterior(ArrayList listaInterior, Nodo n){
            
            for(int i=0;i<listaInterior.size();i++){
                if(n.equals((Nodo)listaInterior.get(i))){
                    return false;}
            }
            
            return true;
        }
        
        /**
         * Comprueba si un nodo está en la listaFrontera
         * @param listaFrontera lista donde están los candidatos a camino
         * @param n nodo a comprobar
         * @return true si NO está en la lista y false SI está en la lista
         */
        public boolean noEnListaFrontera(ArrayList listaFrontera, Nodo n){
            
            for (int i=0;i<listaFrontera.size();i++){
                if(n.equals((Nodo)listaFrontera.get(i))){
                    return false;}
            }
            
            return true;
        }
        
        //Función utilizada para la parte de lógica difusa donde se le indica el siguiente punto al que debe ir el robot.
        //Busca cual es el punto más cercano.
        public Point3d puntoMasCercano(Point3d posicion){
            int inicio;
            Point3d punto = new Point3d(posicion);
            double distancia;
            double cerca = 100;

            inicio = (int) (tamaño-(posicion.z+(tamaño/2)));
            
            for(int i=0; i<tamaño; i++)
                for(int j=inicio+1; j<tamaño; j++){
                    if(camino[i][j]=='X'){
                        distancia = Math.abs(posicion.x+(tamaño/2)-i) + Math.abs(tamaño-(posicion.z+(tamaño/2))-j);
                        if(distancia < cerca){
                            punto.x=i;
                            punto.z=j;
                            cerca = distancia;
                        }
                    }
                }

            return punto;
        }

        /** This method is called by the simulator engine on reset. */
    @Override
        public void initBehavior() {

            System.out.println("Entra en initBehavior");
            //Calcula A*
            int a = AEstrella();

            if(a!=0){
                System.err.println("Error en el A*");
            }else{
                // init controller
                controller = new FuzzyController();
            }
        }

        /** This method is call cyclically (20 times per second)  by the simulator engine. */
    @Override
        public void performBehavior() {

            double angulo;
            int giro;

            //Ponemos las lecturas de los sonares al controlador difuso
            //System.out.println("Fuzzy Controller Input:");
            float[] sonar = new float[9];
            for(int i=0; i<9; i++){
                if(sonars.getMeasurement(i)==Float.POSITIVE_INFINITY){
                    sonar[i] = sonars.getMaxRange();
                } else {
                    sonar[i] = (float) sonars.getMeasurement(i);
                }

                //System.out.println("    > S"+ i +": " + sonar[i]);
            }

     
            //Calcula ángulo del robot
            Transform3D rotTrans = new Transform3D();
            this.rotationGroup.getTransform(rotTrans); //Obtiene la transformada de rotación

            //Debe calcular el ángulo a partir de la matriz de transformación
            //Nos quedamos con la matriz 3x3 superior
            Matrix3d m1 = new Matrix3d();
            rotTrans.get(m1);

            //Calcula el ángulo sobre el eje y
            angulo = -java.lang.Math.asin(m1.getElement(2,0));

            if(angulo<0.0)
                angulo += 2*Math.PI;
            assert(angulo>=0.0 && angulo<=2*Math.PI);

            //Calcula la dirección
            if(m1.getElement(0, 0)<0)
                angulo = -angulo;
            angulo = angulo*180/Math.PI;            
            if(angulo<0 && angulo>-90)
                angulo += 180;
            if(angulo<-270 && angulo>-360)
                angulo += 180+360;


            //Calcula el siguiente punto al que debe ir del A*
            Point3d coord = new Point3d();
            this.getCoords(coord);

            Point3d punto = puntoMasCercano(coord);
            coord.x = coord.x+(tamaño/2);
            coord.z = tamaño-(coord.z+tamaño/2);
            coord.x = (int)coord.x;
            coord.z = (int)coord.z;
            
            
            //Calcula distancia y ángulo del vector, creado desde el punto que se encuentra el robot,
            //hasta el punt que se desea ir
            double distan = Math.sqrt(Math.pow(coord.z-punto.z, 2)+Math.pow(coord.x-punto.x, 2));    
            double phi= Math.atan2((punto.z-coord.z),(punto.x - coord.x));
            phi = phi*180/Math.PI;
            
            //Calcula el giro que debe realizar el robot. Este valor es el que se le pasa al controlador difuso.
            double rot = phi-angulo;
            if(rot<-180)
                rot += 360;
            if(rot>180)
                rot -=360;
            
            //System.out.println("Angulo de giro: "+rot);
            
            //Ejecuto el controlador
            controller.step(sonar, rot);

            //Obtengo las velocidades calculadas y las aplico al robot
            setTranslationalVelocity(controller.getVel());
            setRotationalVelocity(controller.getRot());

            //Para mostrar los valores del controlador
            System.out.println("Fuzzy Controller Output:");
            System.out.println("    >vel: "+ controller.getVel());
            System.out.println("    >rot: "+ controller.getRot());
        }

}
